/*
Here is a file containing a sequence of integers separated by spaces.
Download it and write a Java program that finds the greatest number in this file.
Enter the result.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GreatestNumber {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("/home/igor/IdeaProjects/hyper_skill/tasks/greatest_number/dataset_91007.txt");
        Scanner scan = new Scanner(file);
        int currentNum;
        int maxNum = 0;

        while (scan.hasNextInt()) {
            currentNum = scan.nextInt();
            maxNum = Math.max(currentNum, maxNum);
        }

        System.out.println(maxNum);
    }
}
