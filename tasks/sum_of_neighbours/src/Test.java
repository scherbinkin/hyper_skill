/*
Write a program, which inputs the rectangular matrix from a sequence of lines, ending with a line,
containing the only word "end" (without the quotation marks).
The program should output the matrix of the same size, where each element in the position (i, j)
is equal to the sum of the elements from the first matrix on the positions (i-1, j)(i+1, j)(i, j-1), (i, j+1).
Boundary elements have neighbours on the opposite side of the matrix. In the case of one row or column,
the element itself may be its neighbour.

Sample Input 1:
9 5 3
0 7 -1
-5 2 9
end

Sample Output 1:
3 21 22
10 6 19
20 16 -1

Sample Input 2:
1
end

Sample Output 2:
4
 */
/*
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.util.Arrays;
import java.util.Scanner;

public class SumOfNeighbours {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("./rect_array.txt");
        Scanner scan = new Scanner(file);

        int row = 0;
        int col = 0;
        String str = "";
        String[] rowArr = null;
        short abc = Short.MAX_VALUE - 1;


        //проход исходного массива по строкам, парсинг его элементов
        while(scan.hasNextLine()) {
            rowArr = scan.nextLine().trim().split("\\s");
            str += Arrays.toString(rowArr).replace("[", "").replace("]", "").trim() + " ";
            col = rowArr.length;
            row++;
            if (scan.hasNext("end")) {
                break;
            }
        }
        scan.close();

        //сборка 2х массива int из строки
        int[][] oldArr = new int[row][col];
        Scanner scan2 = new Scanner(str.replace(",", ""));
        for(int i = 0; i < row; i++) {
            for(int j = 0; j < col; j++) {
                oldArr[i][j] = scan2.nextInt();
            }
        }
        scan2.close();

        //копируем крайние строки/столбцы дополняя матрицу необходимыми "соседями"
        int[][] newArr = new int[row + 2][col + 2];
        for (int i = 1; i <= row; i++ ) {
            for (int j = 1; j <= col; j++) {
                newArr[i][j] = oldArr[i - 1][j - 1];
                newArr[row + 1][j] = oldArr[row - i][j - 1];
                newArr[0][j] = oldArr[row - 1][j - 1];
                newArr[i][col + 1] = oldArr[i - 1][0];
                newArr[i][0] = oldArr[i - 1][col - 1];
            }
        }

        //реализация логики(i-1, j)(i+1, j)(i, j-1), (i, j+1), вывод готового массива;
        for (int i = 1; i <= row; i++ ) {
            for (int j = 1; j <= col; j++) {
                System.out.print(newArr[i - 1][j] + newArr[i + 1][j] + newArr[i][j - 1] + newArr[i][j + 1] + " ");
            }
            System.out.print("\n");
        }
    }
}
*/
public class Test {

    public static void main(String args[]) {

        Person person = new Person("R. Johnson");

        System.out.println(Person.getNextId()); // (1)
    }
}

class Person {

    private static long nextId = 1;

    long id;
    String name;

    public Person(String name) {
        this.name = name;
        this.id = nextId;
        nextId++; // (2)
    }

    public static long getNextId() { // (3)
        return nextId;
    }
}