/*
Write a program that reads an array of strings and checks whether the array is in alphabetical order or not.
There are several rules to do it:
1) You can compare chars with < to see if one comes before the other one (i.e. by comparing ASCII values).
2) Shorter strings come before longer strings whenever the shorter string is a subset of the longer one.
So, "a" comes before "abc".
3) Finally, strings which are identical are in alphabetical order.
Input data format
The single input line contains strings separated by spaces.
Output data format
Only a single value - true or false.
 */

import java.util.*;

public class AlphabeticalOrder {
    public static void main(String[] args) {
        //variant from hyperskill:
//        Scanner scanner = new Scanner(System.in);
//        String[] arr = scanner.nextLine().trim().split(" ");
//
//        boolean alphabetical = true;
//        String lastOne = arr[0];
//        for (int i = 1; i < arr.length; i++) {
//            if (arr[i].compareTo(lastOne) < 0) {
//                System.out.println(arr[i].compareTo(lastOne));
//                alphabetical = false;
//                break;
//            }
//            System.out.println(arr[i].compareTo(lastOne));
//            lastOne = arr[i];
//        }
//
//        System.out.println(alphabetical);
//    }
//}

        Scanner scan = new Scanner(System.in);

        String current = "";
        String previous = "";
        char subCurrent = 'a';
        char subPrevious = 'a';
        boolean result = true;
        int summCurrent = 0;
        int summPrevious = 0;

        while (scan.hasNext()) {
            current = scan.next();
            if (current.length() == 1 && previous.length() == 1) {     // 1st condition
                if (current.charAt(0) >= previous.charAt(0)) {
                    previous = current;
                } else {
                    result = false;
                    break;
                }
            } else if (current.length() > 0 && previous.length() > 0) {     //2nd condition
                if (previous.length() < current.length()) {
                    for (int i = 0; i < previous.length(); i++) {
                        if (previous.charAt(i) == current.charAt(i)) {
                        } else {
                            result = false;
                            break;
                        }
                    }
                    previous = current;
                } else if (previous.length() == current.length())  {
                    subCurrent = 'a';
                    subPrevious = 'a';
                    for (int i = 0; i < current.length(); i++) {
                        if (current.charAt(i) >= subCurrent && previous.charAt(i) >= subPrevious) {
                            subCurrent = current.charAt(i);
                            subPrevious = previous.charAt(i);
                            summCurrent += subCurrent;
                            summPrevious += subPrevious;
                        } else {
                            result = false;
                            break;
                        }
                    }
                    if (summCurrent >= summPrevious) {
                        previous = current;
                    }else {
                        result = false;
                        break;
                    }
                } else {
                    result = false;
                    break;
                }
            } else {                // 3rd condition
                previous = current;
            }
        }
        System.out.println(result);
    }
}