class A {

    protected int a;
    protected int b;
//    protected int b;
}

class B extends A {

    protected int b;

    public B(int a, int b) {
        super();     // 1
        super.a = a; // 2
        this.b = b; // 3
    }
}

class C extends B {

    protected int c;

    public C(int a, int b) {
        super(a, b); // 4
    }

    public C(int a) {
        super(a, 10); // 5
    }
}

class Test {
    public static void main(String[] args) {
        C ce = new C(2, 2);
        System.out.println(ce.a + " " + ce.b);
        char ch = '\u0031' + 5;
        System.out.println(ch);
    }
}