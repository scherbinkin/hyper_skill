/*
Write a program that takes a date string formatted as YYYY-MM-DD as input, then converts and outputs it as MM/DD/YYYY.
For instance, the input 2007-07-21 will result in the following output 07/21/2007.
The program must print "Incorrect input" if the date is not possible.
Assume that every month has 31 days and that the year, month or day cannot be 0 (or 00).
Sample Input 1:
2012-09-28
Sample Output 1:
09/28/2012
 */

import java.util.*;

public class ConvertADate {
    public static void main(String[] args) {
        String[] date = new Scanner(System.in).nextLine().trim().split("\\D");

        if (Integer.parseInt(date[1]) > 12 || Integer.parseInt(date[2]) > 31) {
            System.out.println("Incorrect input");
        } else {
            System.out.println(date[1] + "/" + date[2] + "/" + date[0]);
        }
    }
}
