import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/*
You want to hack a website and get all available parameters. Print them "key : value".
If you find the password (parameter pass), you should print it after all parameters like in the sample.
If a parameter doesn't have value print "not found".

Note: the order of parameters should like as in the url.
If a url does not contain parameter pass, do not print anything after parameters

Advice: learn the structure of URL.

Sample Input 1:
https://target.com/index.html?pass=12345&port=8080&cookie=&host=localhost

Sample Output 1:
pass : 12345
port : 8080
cookie : not found
host : localhost
password : 12345
 */
public class ParseUrl {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("./url.txt");
        String[] url = new Scanner(file).nextLine().split("[?,&*=]"); //ищем совпадения вида ?*= или &*=
        String password = "";

        for (int i = 1; i < url.length; i += 2) {
            if (i == url.length - 1 || url[i + 1].isEmpty()) {
                System.out.println(url[i] + " : not found");
            } else {
                System.out.println(url[i] + " : " + url[i + 1]);
                password = "pass".equals(url[i]) ? url[i + 1] : password;
            }
        }

        System.out.println(password.isEmpty() ? password : "password : " + password);
    }
}
