/*
t this stage, you need to add the ability to read and write original and cipher data to files.
The program must parse two additional arguments -in and -out to specify the full name of a file
to read data and to write the result. Arguments -mode, -key, and -data should still work as before.
Your program should read data from -data or from a file written in the -in argument. That's why you
can't have both -data and -in arguments simultaneously, only one of them.

    If there is no -mode, the program should work in enc mode.
    If there is no -key, the program should consider that key = 0.
    If there is no -data, and there is no -in the program should assume that the data is an empty string.
    If there is no -out argument, the program must print data to the standard output.
    If there are both -data and -in arguments, your program should prefer -data over -in.

If there is a non-standard situation (an input file does not exist or an argument doesn’t have a value),
the program should not fail. Instead, it must display a clear message about the problem and stop successfully.
The message should contain the word "Error" in any case.
 */

package encrypt_decrypt;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        try {
            String argsOfMode = "enc";
            int argsOfKey = 0;
            String argsOfData = "";
            String argsOfOutput = "";
            String argsOfInput = "";
            char[] arrayOfData;
//parsing String[] args
            for (int i = 0; i < args.length; i++) {
                switch (args[i]) {
                    case "-mode":
                        argsOfMode = args[i + 1];
                        i++;
                        break;
                    case "-key":
                        try {
                            argsOfKey = Integer.parseInt(args[i + 1]);
                            i++;
                            break;
                        } catch (Exception e) {
                            System.out.println("Error format data in the field -key --> " + args[i + 1]);
                            i = args.length;
                        }
                        break;
                    case "-data":
                        argsOfData = args[i + 1];
                        i++;
                        break;
                    case "-in":
                        argsOfInput = args[i + 1];
                        i++;
                        break;
                    case "-out":
                        argsOfOutput = args[i + 1];
                        i++;
                        break;
                    default:
                        i++;
                        break;
                }
            }
//check priority -mode
            if (argsOfData.length() > 0) {
                arrayOfData = argsOfData.toCharArray();
            } else if (argsOfInput.length() > 0) {
                arrayOfData = new Scanner(new File("./" + argsOfInput)).nextLine().toCharArray();
            }
            else {
                arrayOfData = argsOfData.toCharArray();
            }

            if (argsOfMode.equals("enc")) {                             //encryption
                for (int i = 0; i < arrayOfData.length; i++) {
                    arrayOfData[i] = (char) (arrayOfData[i] + argsOfKey);
                }
            } else {                                                    //decryption
                for (int i = 0; i < arrayOfData.length; i++) {
                    arrayOfData[i] = (char) (arrayOfData[i] - argsOfKey);
                }
            }
//output ciphertext
            if (argsOfOutput.length() > 0) {
                try (PrintWriter printWriter = new PrintWriter(new File("./" + argsOfOutput))) {
                    printWriter.println(String.valueOf(arrayOfData));
                } catch (IOException e) {
                    System.out.printf("Error %s", e.getMessage());
                }
            } else {
                System.out.println(String.valueOf(arrayOfData));
            }

        } catch (FileNotFoundException e) {
            System.out.printf("Error any trouble with file %s", e.getMessage());
        } catch (Exception e) {
            System.out.println("Error other exception");
        }
    }
}
