import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
/*
A right rotation is an operation which shifts each element of the array to the right.
For example, if a right rotation is 1 and array is {1,2,3,4,5}, the new array will be {5,1,2,3,4}.
Another example, if a right rotation is 2 and array {1,2,3,4,5}, the new array will be {4,5,1,2,3}, because
{1,2,3,4,5} ->  {5,1,2,3,4} ->  {4,5,1,2,3}.

Sample Input 1:
1 2 3 4 5
1

Sample Output 1:
5 1 2 3 4

Sample Input 2:
1 2 3 4 5
2

Sample Output 2:
4 5 1 2 3

Sample Input 3:
1 2 3 4 5
8

Sample Output 3:
3 4 5 1 2
 */
public class RightRotation {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("array.txt");
        Scanner scan = new Scanner(file);

        String[] array = scan.nextLine().split("\\s");
        long shiftArray = scan.nextInt();
        shiftArray = shiftArray > array.length ? shiftArray % array.length : shiftArray; //max speed
        scan.close();

        String buffer;
        String bufferNext;

        for (int i = 0; i < shiftArray; i++) {
            buffer = array[0];
            array[0] = array[array.length - 1];
            for (int j = 1; j < array.length; j++) {
                bufferNext = array[j];
                array[j] = buffer;
                buffer = bufferNext;

            }
        }
        for (String element : array) {
            System.out.printf("%s ",element);
        }
    }
}
/*      //test 5 failed
        String seq = scan.nextLine().trim();
        int shiftSeq = scan.nextInt();

        for (int i = 0; i < shiftSeq; i++) {
            seq = seq.substring(seq.length() - 1).trim() + " " + seq.substring(0, seq.length() - 1).trim();
        }
        System.out.println(seq);
    }
}
*/

/*      //Slowly
        String[] array = scan.nextLine().split("\\s");
        int shiftArray = scan.nextInt();
        scan.close();

        String buffer;
        String bufferNext;

        for (int i = 0; i < shiftArray; i++) {
            buffer = array[0];
            array[0] = array[array.length - 1];
            for (int j = 1; j < array.length; j++) {
                bufferNext = array[j];
                array[j] = buffer;
                buffer = bufferNext;

            }
        }
        for (String element : array) {
            System.out.printf("%s ",element);
        }
    }
}
*/