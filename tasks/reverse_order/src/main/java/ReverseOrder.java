/*
Write a program that reads three strings and outputs them in the
reverse order, each in a new line.
 */

import java.util.Scanner;

public class ReverseOrder {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String scanWord = "";
        while(scan.hasNext()) {
            scanWord += scan.next() + " ";
        }
        String[] arrWord = scanWord.trim().split(" ");

        for (int i = arrWord.length - 1; i >= 0; i--) {
            System.out.println(arrWord[i]);
        }
    }
}