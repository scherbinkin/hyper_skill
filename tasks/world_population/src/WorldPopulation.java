/*
Here's a file that stores data on the world population since 1950, according to the United States Census Bureau (2017).
Download the file and write a Java program to find out in what year the largest increase in population occurred
as compared to the previous year.
The file has two columns: year and population. Take a look at it to understand the format better.
 */


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class WorldPopulation {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("/home/igor/IdeaProjects/hyper_skill/tasks/world_population/dataset_91069.txt");
        Scanner scan = new Scanner(file);

        short currentYear;
        short maxYear = 0;
        long currentPop;
        long maxPop = 0L;
        scan.nextLine();
        scan.nextShort();
        long lastPop = Long.parseLong(scan.next().replace(",", ""));

        while (scan.hasNext()) {
            currentYear = scan.nextShort();
            currentPop = Long.parseLong(scan.next().replace(",", ""));
            if (currentPop - lastPop > maxPop) {
                maxYear = currentYear;
                maxPop = currentPop - lastPop;
            }
            lastPop = currentPop;
//            lastYear = currentYear;
        }

        System.out.println(maxYear + " " + maxPop);
    }
}
