package encryptdecrypt;

class ParsingCommandLine {

    private String[] args;

    ParsingCommandLine(String[] args) {

        this.args = args;
    }

    void parsingCommandLine() {

        for (int i = 0; i < args.length; i += 2) {
            switch (args[i]) {
                case "-mode":
                    Crypting.mode = args[i + 1];
                    break;
                case "-key":
                    try {
                        Crypting.key = Integer.parseInt(args[i + 1]);
                        break;
                    } catch (Exception e) {
                        System.out.println("Error format data in the field -key --> " + args[i + 1]);
                        i = args.length;
                    }
                    break;
                case "-data":
                    Crypting.data = args[i + 1];
                    break;
                case "-in":
                    Crypting.inputFile = args[i + 1];
                    break;
                case "-out":
                    Crypting.outputFile = args[i + 1];
                    break;
                case "-alg":
                    Crypting.algorithm = args[i + 1];
                    break;
                default:
                    break;
            }
        }
    }
}
