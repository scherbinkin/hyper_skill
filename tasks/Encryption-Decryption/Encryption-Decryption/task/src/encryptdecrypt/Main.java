package encryptdecrypt;

/**
 *The program must parse two additional arguments -in and -out to specify the full name of a file
 * to read data and to write the result. Arguments -mode, -key, and -data should still work as before
 * (The first argument should determine the program’s mode (enc - encryption, dec - decryption).
 * The second argument is an integer key to modify the message, and
 * the third argument is a text or ciphertext within quotes to encrypt or decrypt..)
 *
 * Your program should read data from -data or from a file written in the -in argument.
 * That's why you can't have both -data and -in arguments simultaneously, only one of them.
 *
 *     If there is no -mode, the program should work in enc mode.
 *     If there is no -key, the program should consider that key = 0.
 *     If there is no -data, and there is no -in the program should assume that the data is an empty string.
 *     If there is no -out argument, the program must print data to the standard output.
 *     If there are both -data and -in arguments, your program should prefer -data over -in.
 *     If there is no -alg you should default it to shift.
 *
 * When starting the program, the necessary algorithm should be specified by an argument (-alg).
 * The first algorithm should be named shift, the second one should be named unicode.
 *
 * Remember that in case of shift algorithm you need to encode only English letters
 * (from 'a' to 'z' as the first circle and from 'A' to 'Z' as the second circle
 * i.e. after 'z' comes 'a' and after 'Z" comes 'A').
 *
 * If there is a non-standard situation (an input file does not exist or an argument doesn’t have a value),
 * the program should not fail. Instead, it must display a clear message about the problem and stop successfully.
 * The message should contain the word "Error" in any case.
 */

public class Main {

    public static void main(String[] args) {

        new ParsingCommandLine(args).parsingCommandLine();
        Crypting crypting;

        if("enc".equalsIgnoreCase(Crypting.mode)) {
            crypting = new Encryption();
            crypting.doCrypting();
        } else if("dec".equalsIgnoreCase(Crypting.mode)) {
            crypting = new Decryption();
            crypting.doCrypting();
        } else {
            crypting = new Encryption();
            crypting.doCrypting();
        }
    }
}

