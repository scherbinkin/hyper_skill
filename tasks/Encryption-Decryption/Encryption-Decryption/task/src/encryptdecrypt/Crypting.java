package encryptdecrypt;

abstract class Crypting {

    void doCrypting() {

        selectSourceData();
        encryptDecrypt();
        outputCiphertext();
    }

    static String mode = "enc";
    static String data = "";
    static String algorithm = "";
    static String outputFile = "";
    static String inputFile = "";
    static char[] arrayOfData;
    static int key = 0;

    public abstract void selectSourceData();

    public abstract void encryptDecrypt();

    public abstract void outputCiphertext();

}
