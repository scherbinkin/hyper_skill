package encryptdecrypt;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Encryption extends Crypting {

    @Override
    public void selectSourceData() {

        try {
            if (data.length() > 0) {
                arrayOfData = data.toCharArray();
            } else if (inputFile.length() > 0) {
                arrayOfData = new Scanner(new File("./" + inputFile)).nextLine().toCharArray();
            }
            else {
                arrayOfData = data.toCharArray();
            }
        } catch (FileNotFoundException e) {
            System.out.printf("Error any trouble with file %s", e.getMessage());
        }
    }

    @Override
    public void encryptDecrypt() {
        if (algorithm.equals("unicode")) {
            for (int i = 0; i < arrayOfData.length; i++) {
                arrayOfData[i] = (char) (arrayOfData[i] + key);
            }
        } else {
            for (int i = 0; i < arrayOfData.length; i++) {
                if ((char) (arrayOfData[i] + key) <= 122) {
                    arrayOfData[i] = arrayOfData[i] == ' ' ? ' ' : (char) (arrayOfData[i] + key);
                } else {
                    arrayOfData[i] = (char) (arrayOfData[i] + key - 26);
                }
            }
        }
    }

    @Override
    public void outputCiphertext() {

        try {
            if (outputFile.length() > 0) {
                try (PrintWriter printWriter = new PrintWriter(new File("./" + outputFile))) {
                    printWriter.println(String.valueOf(arrayOfData));
                } catch (IOException e) {
                    System.out.printf("Error %s", e.getMessage());
                }
            } else {
                System.out.println(String.valueOf(arrayOfData));
            }
        } catch (Exception e) {
            System.out.println("Error other exception");
        }
    }
}
